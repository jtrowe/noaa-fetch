
use Log::Log4perl qw( :easy );
use Test::More;

BEGIN {
    use_ok 'Report';
}

Log::Log4perl->easy_init($DEBUG);

my $input = '1/85210/20130724T000000/content.xml';
my $xml;

if ( open my $FH, '<', $input ) {
    $xml = join '', <$FH>;
    close $FH;
}
else {
    die "cannot open file: $!";
}

Report->new->objectify($xml);

ok 1;

done_testing();

