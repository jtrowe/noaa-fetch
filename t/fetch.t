use strict;
use warnings;

use Test::More;

BEGIN {
    use_ok qw( Fetch );
}

my %goodParam = (
    zipCodeList => '85210',
    begin       => '0',
);

# testing createUrl
{
    my $url = Fetch::createUrl(%goodParam);
    ok $url, 'url was returned';
    diag 'url=' . $url;
}

# testing fetch
{
    my $response = Fetch::fetch(%goodParam);
    ok $response, 'response was returned';
    ok ref($response) =~ 'HTTP::Response', 'Got the right class response';
}

done_testing();

