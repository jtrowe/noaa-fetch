package Fetch;

use strict;
use warnings;

use Data::Dumper;
use LWP::UserAgent;

sub fetch {
    my %args = @_;

    my $url = createUrl(%args);

    unless ( $url ) {
        die 'could not construct a url';
    }

    my $agent = LWP::UserAgent->new;

    return $agent->get($url);
}

sub createUrl {
    my %args = @_;

    $args{baseUrl} = 'http://graphical.weather.gov/xml/sample_products/browser_interface/ndfdXMLclient.php';

    #2 options
    $args{product} ||= 'time-series';
    #$args{product} = 'glance';

    # e is default.
    #$args{'Unit'} = 'e';
    #$args{'Unit'} = 'm';

    #$args{'maxt'} = 'maxt';
    #$args{'mint'} = 'mint';

#    zipCodeList=20910+25414&product=time-series&begin=2004-01-01T00:00:00&end=2013-04-21T00:00:00&maxt=maxt&mint=mint

#    my $format = '%s?zipCodeList=%s&product=%s&begin=%s&end=%s&maxt=%s&mint=%s';
#    my $format = '?zipCodeList=%s&product=%s&begin=%s&end=%s';
    my $format = '?zipCodeList=%s&product=%s'; #&begin=%s';

#        baseUrl

    my @keys = qw(
        zipCodeList
        product
    );
#        begin

    foreach my $p ( @keys ) {
        unless ( defined $args{$p} ) {
            die "need param '$p'";
        }
    }

    my @values = @args{ @keys };

#        end
#        Unit
#        maxt
#        mint

#    print STDERR 'DUMP: args=' . Dumper(\%args) . "\n";

    return $args{baseUrl} . ( sprintf $format, @values );
}

1;

