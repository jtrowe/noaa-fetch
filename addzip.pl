#!/usr/bin/env perl

use strict;
use warnings;

use Schema;

my $dsn = 'DBI:SQLite:noaa.db';
my $schema = Schema->connect($dsn, undef, undef, {
    RaiseError => 0,
    unsafe     => 1,
});

foreach my $z ( @ARGV ) {
    if ( $z !~ m/^\d{5}/ ) {
        print STDERR "'" . $z
                . "' does not look like a valid zip code.  Ignoring.\n";
        next;
    }

    my $row = $schema->resultset('Zipcode')->find_or_create({
        zip => $z,
    });

    if ( $row ) {
        print "Zipcode '" . $row->zip . "' is now in the database.\n";
    }
}

