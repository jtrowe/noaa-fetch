
CREATE TABLE zipcode (
    zipcode_id INTEGER AUTO INCREMENT PRIMARY KEY,
    zip VARCHAR(5)
);

CREATE TABLE request (
    request_id INTEGER AUTO INCREMENT PRIMARY KEY,
    zipcode_id INTEGER NOT NULL,
    -- local epoch ( should be utc epoch but math is hard )
    timestamp INTEGER
);

CREATE TABLE response (
    response_id INTEGER AUTO INCREMENT PRIMARY KEY,
    request_id INTEGER NOT NULL,
    -- local epoch ( should be utc epoch but math is hard )
    timestamp INTEGER
);

