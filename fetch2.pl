#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;
use Fetch;
use Getopt::Long;
use LWP::UserAgent;
use Schema;
use XML::Simple;

my $dsn = 'DBI:SQLite:noaa.db';
my $schema = Schema->connect($dsn, undef, undef, {
    RaiseError => 0,
    unsafe     => 1,
});

foreach my $z ( $schema->resultset('Zipcode')->all ) {
    processZip($z);
}

sub processZip {
my $z = shift;
print 'id=' . $z->zipcode_id . "\n";
}
__END__
    my @gmtime = gmtime;
    my $date = sprintf '%d-%02d-%02d', ( 1900 + $gmtime[5] ),
            ( 1 + $gmtime[4] ), $gmtime[3];

    my $zip = shift;

    my %params = (
        zipCodeList => $zip,

    #    product => 'glance',

        #time in UTC
        begin => $date . 'T00:00:00',

    #    begin => '2013-01-02T01:00:00',
    #    end => '2013-01-02T13:00:00',
    );

    my $url = createUrl(%params);

    #print 'url=' . $url . "\n";

    my $agent = LWP::UserAgent->new;

    my $requestRecord = $schema->resultset('Request')->create({
        zipcode_id => $zip->zipcode_id,
        timestamp  => time(),
    });

    sleep 10;

    my $responseRecord = $schema->resultset('Response')->create({
        request_id => $requestRecord->id,
        timestamp  => time(),
    });

if ( 0 ) {
    my $response = $agent->get($url);
    print 'Status: ' . $response->status_line . "\n";

    #FIXME maybe decode?
    my $xml = $response->content;

    #print 'Content: ' . "\n";
    #print $response->content;
    #print '/Content: ' . "\n";
    #print "\n";
}

} # processZip

sub createUrl {
    my %args = @_;

    $args{baseUrl} = 'http://graphical.weather.gov/xml/sample_products/browser_interface/ndfdXMLclient.php';

    #2 options
    $args{product} ||= 'time-series';
    #$args{product} = 'glance';

    # e is default.
    #$args{'Unit'} = 'e';
    #$args{'Unit'} = 'm';

    #$args{'maxt'} = 'maxt';
    #$args{'mint'} = 'mint';

#    zipCodeList=20910+25414&product=time-series&begin=2004-01-01T00:00:00&end=2013-04-21T00:00:00&maxt=maxt&mint=mint

#    my $format = '%s?zipCodeList=%s&product=%s&begin=%s&end=%s&maxt=%s&mint=%s';
#    my $format = '?zipCodeList=%s&product=%s&begin=%s&end=%s';
    my $format = '?zipCodeList=%s&product=%s&begin=%s';

#        baseUrl

    my @keys = qw(
        zipCodeList
        product
        begin
    );

#        end
#        Unit
#        maxt
#        mint

    print STDERR 'DUMP: args=' . Dumper(\%args) . "\n";

    return $args{baseUrl} . ( sprintf $format, @args{ @keys } );
}

sub objectify {
    my %args = @_;

    my $params = $args{'params'};
    my $xml = $args{'xml'};

    my $x = XMLin($xml);

    mapTimeLayouts($x);

    my $o = $x->{data}{parameters}{temperature};

    foreach my $k ( keys %{ $o } ) {
        foo($k, $x, $o);
    }

    print STDERR 'object: ' . Dumper($o) . "\n";

    return $o;
}

sub mapTimeLayouts {
    my $x = shift;

    my %hash;

    my $p = $x->{data}{'time-layout'};
    foreach my $val ( @{ $p } ) {
        my $key = $val->{'layout-key'};
        $hash{$key} = $val;
    }

    $x->{data}{'time-layout'} = \%hash;
}

sub foo {
    my $chunkName = shift;
    my $x = shift;
    my $o = shift;

#    print STDERR 'chunkName=' . $chunkName . "\n";

    my $layoutKey = $o->{$chunkName}{'time-layout'};

#    print STDERR "\t" . 'layoutKey=' . $layoutKey . "\n";

    my $times = $x->{'data'}{'time-layout'}{$layoutKey};

#    print STDERR 'p: ' . Dumper($p) . "\n";

#    print STDERR 'times: ' . Dumper($times) . "\n";
    if ( $times ) {
        $o->{$chunkName}{'jr_times'} = $times->{'start-valid-time'};
    }

}

