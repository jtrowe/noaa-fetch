package Report;

use Data::Dumper;
use DateTime::Format::Strptime;
use Log::Log4perl qw( get_logger );
use XML::LibXML;

sub new {
    my $class = shift;

    my $self = {};

    bless $self, $class;

    return $self;
}

sub log {
    my ( $self ) = @_;

    unless ( $self->{_log} ) {
        $self->{_log} = get_logger(ref $self);
    }

    return $self->{_log};
}

sub objectify {
    my $self = shift;
    my $xml = shift;

    my $doc = XML::LibXML->load_xml(string => $xml);

    # /dwml/data/

    my $timeLayout = {};

    my @nodes = $doc->getElementsByTagName('time-layout');
    foreach my $node ( @nodes ) {
        $self->timeLayout($timeLayout, $doc, $node);

#        $self->log->debug('timeLayout=' . Dumper($timeLayout));
    }

    $self->log->debug('timeLayouts=' . join(', ', keys %{ $timeLayout }));

    my @pNodes = $doc->getElementsByTagName('parameters');
    $self->log->debug('parameters node count=' . @pNodes);

    # > 1, probably bad

    @nodes = $pNodes[0]->getElementsByTagName('temperature');

    foreach my $n ( @nodes ) {
        my $type = $n->getAttribute('type');
        $self->log->debug('type=' . $type);

        unless ( 'hourly' eq $type ) {
            next;
        }

        my $l = $n->getAttribute('time-layout');
        $self->log->debug('l=' . $l);

        my @c = $n->childNodes;

#        if ( ( ! @c ) == ( ! num of layout ) ) { error }

        my $index = 0;
        foreach my $c ( @c ) {
            unless ( XML_ELEMENT_NODE == $c->nodeType ) {
                next;
            }

            my $times = $timeLayout->{$l}->[$index];
            my $times_s = scalar( @{ $times } );

            my $x = join ' :: ', @{ $times };

            my $tagName = $c->tagName;
            my $text = $self->concatTextNodeValues($c);
            $self->log->debug("temp: tagName=$tagName ; text=$text"
            . ' ; times_s=' . $times_s . ' x=' . $x
            );

            $index++;
        }

#FIXME dev
last;
    }

}

sub timeLayout {
    my ( $self, $layout, $doc, $element ) = @_;

    my $dtParser = DateTime::Format::Strptime->new(
        pattern => '%Y-%m-%dT%H:%M:%S%z',
    );

    my @times;
    my @duration;

    foreach my $c ( $element->childNodes ) {
        unless ( XML_ELEMENT_NODE == $c->nodeType ) {
            next;
        }

        my $tagName = $c->tagName;
        my $text = $self->concatTextNodeValues($c);
        $self->log->debug("layout: tagName=$tagName ; text=$text");

        if ( 'layout-key' eq $tagName ) {
            $layout->{$text} = \@times;
        }
        elsif ( 'start-valid-time' eq $tagName ) {
            $text =~ s/-(\d{2}):(\d{2})$/-$1$2/;

            my $dt = $dtParser->parse_datetime($text);
            $dt->set_formatter($dtParser);

            if ( @duration ) {
                # some time-layout blocks do not have send-valid-time

                # append the start time
                push @times, [ @duration ];
                @duration = ();
            }

            push @duration, $dt;

        }
        elsif ( 'end-valid-time' eq $tagName ) {
            $text =~ s/-(\d{2}):(\d{2})$/-$1$2/;

            my $dt = $dtParser->parse_datetime($text);
            $dt->set_formatter($dtParser);

            if ( 1 != @duration ) {
                die 'bad number of elements in duration when finding end-valid-time';
            }

            push @duration, $dt;

            # append the start-end times
            push @times, [ @duration ];
            @duration = ();
        }

    }

    return $layout;
}

sub concatTextNodeValues {
    my ( $self, $e ) = @_;

    my $text = '';

    foreach my $n ( $e->childNodes ) {
        unless ( XML_TEXT_NODE == $n->nodeType ) {
            die 'Found something other than text node';
        }

        $text .= $n->data;
    }

    return $text;
}

1;

