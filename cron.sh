#!/bin/bash

## These 3 lines are mandatory.
export PERLBREW_ROOT=$HOME/perl5/perlbrew
export PERLBREW_HOME=$HOME/.perlbrew
source ${PERLBREW_ROOT}/etc/bashrc

## Do stuff with 5.14.1
perlbrew use perl-5.14.1
cd $HOME/projects/noaa_fetch/

# home
perl fetch_job.pl 6 85210

# dojo
perl fetch_job.pl 6 85281

# south mountain
perl fetch_job.pl 6 85042

# grand canyon national park
perl fetch_job.pl 6 86023

# downtown fort wayne
perl fetch_job.pl 6 46802
