package Schema;

use strict;
use warnings;

use base qw( DBIx::Class::Schema::Loader );

__PACKAGE__->naming('current');

1;
