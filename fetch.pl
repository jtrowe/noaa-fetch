#!/usr/bin/env perl

#NOTES
#   overall
#       source: http://graphical.weather.gov/xml/
#       maybe keep track of points in relation to zip code?
#   
#   xml
#       /dwml/head/product/create-date might be useful
#
#   RTMA vs NDFD    -   actual vs predicted data
#


use strict;
use warnings;

use Data::Dumper;
use Fetch;
use Getopt::Long;
use LWP::UserAgent;
use Schema;
use XML::Simple;

my $dsn = 'DBI:SQLite:noaa.db';
my $schema = Schema->connect($dsn, undef, undef, {
    RaiseError => 0,
    unsafe     => 1,
});

my $xml;

my %opts = (
);

GetOptions(
    'input=s'  => \$opts{'input'},
    'output=s' => \$opts{'output'},
);

if ( $opts{'input'} ) {
    if ( open my $I, '<', $opts{'input'} ) {
        my @a = <$I>;

        close $I;

        $xml = join '', @a;
    }
}

foreach my $z ( $schema->resultset('Zipcode')->all ) {
    my $params;
    ( $xml, $params ) = processZip($z->zip);
    my $object = objectify(
        params => $params,
        xml    => $xml,
    );
}

sub processZip {
    my @gmtime = gmtime;
    my $date = sprintf '%d-%02d-%02d', ( 1900 + $gmtime[5] ),
            ( 1 + $gmtime[4] ), $gmtime[3];
    #print 'date=' . $date . "\n";
    #exit;

    my $zip = shift;

    my %params = (
        zipCodeList => $zip,

    #    product => 'glance',

        #time in UTC
        begin => $date . 'T00:00:00',

    #    begin => '2013-01-02T01:00:00',
    #    end => '2013-01-02T13:00:00',
    );

    my $url = Fetch::createUrl(%params);

    #print 'url=' . $url . "\n";

    unless ( $xml ) {
        my $agent = LWP::UserAgent->new;

        my $response = $agent->get($url);
        print 'Status: ' . $response->status_line . "\n";

        #FIXME maybe decode?
        $xml = $response->content;

        if ( $opts{'output'} ) {
            if ( open my $OUT, '>', $opts{'output'} ) {
                print $OUT $xml;
                close $OUT;
            }
            else {
                print STDERR 'ERROR: ' . $! . "\n";
                exit;
            }
        }

        #print 'Content: ' . "\n";
        #print $response->content;
        #print '/Content: ' . "\n";
        #print "\n";
    }

    return ( $xml, \%params );
} # processZip

sub objectify {
    my %args = @_;

    my $params = $args{'params'};
    my $xml = $args{'xml'};

    my $x = XMLin($xml);

    mapTimeLayouts($x);

    my $o = $x->{data}{parameters}{temperature};

    foreach my $k ( keys %{ $o } ) {
        foo($k, $x, $o);
    }

    print STDERR 'object: ' . Dumper($o) . "\n";

    return $o;
}

sub mapTimeLayouts {
    my $x = shift;

    my %hash;

    my $p = $x->{data}{'time-layout'};
    foreach my $val ( @{ $p } ) {
        my $key = $val->{'layout-key'};
        $hash{$key} = $val;
    }

    $x->{data}{'time-layout'} = \%hash;
}

sub foo {
    my $chunkName = shift;
    my $x = shift;
    my $o = shift;

#    print STDERR 'chunkName=' . $chunkName . "\n";

    my $layoutKey = $o->{$chunkName}{'time-layout'};

#    print STDERR "\t" . 'layoutKey=' . $layoutKey . "\n";

    my $times = $x->{'data'}{'time-layout'}{$layoutKey};

#    print STDERR 'p: ' . Dumper($p) . "\n";

#    print STDERR 'times: ' . Dumper($times) . "\n";
    if ( $times ) {
        $o->{$chunkName}{'jr_times'} = $times->{'start-valid-time'};
    }

}

