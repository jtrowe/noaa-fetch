use strict;
use warnings;

use DateTime;
use Fetch;
use File::Path qw( make_path );
use Getopt::Long;
use HTTP::Request;

my $root = shift @ARGV || 1;
my $zip = shift @ARGV || '85210';

my $now = DateTime->now;
my $s = $now->strftime('%Y%m%d_%H%M%S');
#print "s=$s\n";

my $yesterday = $now->clone->subtract(days => 1)->truncate(to => 'day');
my $y = $yesterday->strftime('%Y%m%d_%H%M%S');
my $urlDateTimeFormat = '%Y%m%dT%H%M%S';
#print "y=$y\n";

my $beginTime = $yesterday->strftime($urlDateTimeFormat);

my $dir = join '/', $root, $zip, $beginTime;

my $noWork = 0;
my %opt;
GetOptions(
    'n' => \$noWork,
);

#if ( $noWork ) {
#{
#    print "dir=$dir\n";
#    exit;
#}

make_path($dir);

my %goodParam = (
    zipCodeList => $zip,
    begin       => $beginTime,
);

my $u = Fetch::createUrl(%goodParam);
my $request = HTTP::Request->new(GET => $u);

use LWP::UserAgent;
my $response = LWP::UserAgent->new->request($request);

if ( $opt{'verbose'} ) {
    print "$zip : response.status_line: " . $response->status_line . "\n";
}

my $file;
my $FH;

$file = join '/', $dir, 'headers.txt';
open $FH, '>', $file;
print $FH $response->headers->as_string;
close $FH;

$file = join '/', $dir, 'content.xml';
open $FH, '>', $file;
print $FH $response->decoded_content;
close $FH;

$file = join '/', $dir, 'request.txt';
open $FH, '>', $file;
print $FH $response->request->as_string;
close $FH;

