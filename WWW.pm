package WWW;

use parent 'Kelp';

sub build {
    my $self = shift;

    my $routes = $self->routes;

    $routes->add('/', 'routeIndex');
    $routes->add('/zip/:zip', 'routeZip');
    $routes->add('/zip/:zip/date/:date', 'routeDate');
}

sub routeIndex {
    my ( $self ) = @_;

    return q|<xhtml>
                <head>
                    <title>Index</title>
                </head>
                <body>
                    <h1>Index</h1>
                    <hr />
                    <a href="/zip/85210/">85210</a>
                </body>
             </xhtml>
    |;
}

sub routeZip {
    my ( $self, $zip ) = @_;

    $zip =~ s/\D+//g;
    $zip = substr $zip, 0, 5;

    my $date = '20130724';

    return qq|<xhtml>
                <head>
                    <title>zip $zip</title>
                </head>
                <body>
                    <h1>zip $zip</h1>
                    <hr />
                    <a href="/">Index</a>
                    <hr />
                    <a href="/zip/$zip/date/$date">$zip / $date</a>
                </body>
             </xhtml>
    |;
}

sub routeDate {
    my ( $self, $zip, $date ) = @_;

    my $file = join '/', '1', $zip, $date . 'T000000', 'content.xml';

    my $FH;
    open $FH, '<', $file;

    my @a = <$FH>;

    close $FH;

#use HTTP::Response;
#
#    my $r = HTTP::Response->new;
#    $r->content_type('application/xml');
#    $r->content(join '', @a);
#    return $r;

    $self->res->set_content_type('application/xml');

    return join '', @a;
}

1;

